package ru.nsu.fit.zbitnev.springTestApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringTestAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringTestAppApplication.class, args);
	}

}
