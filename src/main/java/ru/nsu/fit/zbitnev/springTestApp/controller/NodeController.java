package ru.nsu.fit.zbitnev.springTestApp.controller;

import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.nsu.fit.zbitnev.springTestApp.model.Node;
import ru.nsu.fit.zbitnev.springTestApp.model.NodeRepository;

import java.util.List;
import java.util.Optional;

@RestController
public class NodeController {
    private NodeRepository repository;

    @Autowired
    public NodeController(NodeRepository nodeRepository) {
        this.repository = nodeRepository;
    }

    @PostMapping(value = "/nodes")
    public ResponseEntity createNode(@RequestParam Node node) {
        var baseNode = repository.save(node);

        return ResponseEntity.status(HttpStatus.OK).body("Created with id = " + baseNode.getId());
    }

    @GetMapping("/nodes")
    public List<Node> getAllNodes() {
        return repository.findAll();
    }

    @GetMapping("/node")
    public ResponseEntity getOneNode(@RequestParam Long id) {
        var node = repository.findById(id);
        System.out.println(id);
        if(node.isEmpty()) {
            return ResponseEntity.status(HttpStatus.OK).body(node);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Not found node with id = "+ id);
        }
    }

    @DeleteMapping("/nodes")
    public ResponseEntity deleteAllNodes() {
        repository.deleteAll();

        return ResponseEntity.status(HttpStatus.OK).body("All nodes was deleted");
    }

    @DeleteMapping("/node")
    public ResponseEntity deleteOneNode(@RequestParam Long id) {
        repository.deleteById(id);

        return ResponseEntity.status(HttpStatus.OK).body("Node with id = " + id + " was deleted");
    }

    @GetMapping("/find")
    public List<Node> findInRadius(
            @RequestParam Double lon,
            @RequestParam Double lat,
            @RequestParam Double radius) {
        return repository.findAllByRadius(lon, lat, radius);
    }
}
