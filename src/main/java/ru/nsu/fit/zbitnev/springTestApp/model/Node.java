package ru.nsu.fit.zbitnev.springTestApp.model;

import lombok.Data;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import com.vladmihalcea.hibernate.type.basic.PostgreSQLHStoreType;


import javax.persistence.*;
import java.util.Map;

@Data
@Entity
@Table(name = "Nodes")
@TypeDef(name = "hstore", typeClass = PostgreSQLHStoreType.class)
public class Node {
    @Id
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column
    private double latitude;

    @Column
    private double longitude;

    @Column
    private Long version;

    @Column
    private String timestamp;

    @Column
    private Long changeset;

    @Column
    private Long uid;

    @Column
    private String username;

    @Type(type = "hstore")
    @Column(columnDefinition = "hstore")
    private Map<String, String> tags;
}
