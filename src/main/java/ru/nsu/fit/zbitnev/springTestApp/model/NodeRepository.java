package ru.nsu.fit.zbitnev.springTestApp.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface NodeRepository extends JpaRepository<Node, Long> {
    @Query(value = "SELECT *, earth_distance(ll_to_earth(?2,?1, ll_to_earth(lat, lon)) as node_distance FROM nodesqw " +
            "WHERE earth_box(ll_to_earth(?2,?1), ?3) @> ll_to_earth(lat, lon) ORDER by node_distance;",
            nativeQuery = true)
    public List<Node> findAllByRadius(Double lon, Double lat, Double radius);
}
