package ru.nsu.fit.zbitnev.springTestApp.services;

import ru.nsu.fit.zbitnev.springTestApp.model.Node;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class NodeParser {
    private XMLStreamReader reader;

    public NodeParser(String relativePath) throws IOException, XMLStreamException {
        try {
            var is = Files.newInputStream(Paths.get(relativePath));

            var factory = XMLInputFactory.newInstance();
            this.reader = factory.createXMLStreamReader(is);
        } catch (IOException | XMLStreamException ioe) {
            throw ioe;
        }
    }

    public List<Node> readNodes(int count) throws XMLStreamException {
        var nodes = new ArrayList<Node>();

        for (int i = 0; i < count; i++) {
            var node = this.readNextNode();
            if (node == null) {
                break;
            }
            nodes.add(node);
        }

        return nodes;
    }

    private void parseOSM() {
        try {
            while (reader.hasNext()) {
                var obj = this.readNextObject(reader);
                if (obj != null) {
                    System.err.println("Readed!");
                }
            }
        } catch (XMLStreamException xse) {
            System.err.println(xse.getMessage());
        }
    }

    private Node readNextNode() throws XMLStreamException {
        while (reader.hasNext()) {
            reader.next();

            if (reader.getEventType() == XMLEvent.START_ELEMENT) {
                var fieldName = reader.getLocalName();
                if (!"node".equals(fieldName)) {
                    continue;
                }

                return this.readNode(reader);
            }
        }

        return null;
    }

    private Node readNextObject(XMLStreamReader reader) throws XMLStreamException {
        reader.next();

        if (reader.getEventType() == XMLEvent.START_ELEMENT) {
            var fieldName = reader.getLocalName();

            if (fieldName.equals("node")) {
                var node = this.readNode(reader);
                return node;
            }
        }

        return null;
    }

    private Node readNode(XMLStreamReader reader) throws XMLStreamException {
        var node = new Node();

        var attributesNum = reader.getAttributeCount();
        for (int i = 0; i < attributesNum; i++) {
            var attributeName = reader.getAttributeLocalName(i);
            var attributeValue = reader.getAttributeValue(i);

            switch (attributeName) {
                case "id":
                    node.setId(Long.parseLong(attributeValue));
                case "lat":
                    node.setLatitude(Double.parseDouble(attributeValue));
                    break;
                case "lon":
                    node.setLongitude(Double.parseDouble(attributeValue));
                    break;
                case "version":
                    node.setVersion(Long.parseLong(attributeValue));
                    break;
                case "timestamp":
                    node.setTimestamp(attributeValue);
                    break;
                case "changeset":
                    node.setChangeset(Long.parseLong(attributeValue));
                    break;
                case "uid":
                    node.setUid(Long.parseLong(attributeValue));
                    break;
                case "user":
                    node.setUsername(attributeValue);
                    break;
                default:
                    break;
            }
        }
        node.setTags(readTags(reader));
        return node;
    }

    private HashMap<String, String> readTags(XMLStreamReader reader) throws XMLStreamException {
        var tags = new HashMap<String, String>();
        String rootName = "node";
        while (!reader.isEndElement() || !rootName.equals(reader.getLocalName())) {
            if (reader.isStartElement() && "tag".equals(reader.getLocalName())) {
                var key = reader.getAttributeValue(0);
                var value = reader.getAttributeValue(1);
                tags.put(key, value);
            }
            reader.next();
        }
        return tags;
    }
}
